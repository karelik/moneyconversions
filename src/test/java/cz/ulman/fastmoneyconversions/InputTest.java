package cz.ulman.fastmoneyconversions;

import org.junit.*;

public class InputTest {

    @Test
    public void testInputVariants() {
        String[] eur_inputs = {"1€ kc","1€"};
        String firstResult = null;
        for (final String input : eur_inputs) {
            final String result = FastMoneyConversions.process(input, false);
            System.out.println(result);
            if (firstResult == null) firstResult = result;
            Assert.assertTrue(result.startsWith("EUR 1.00 = CZK "));
            Assert.assertEquals(firstResult, result);
        }
        firstResult = null;
        String[] usd_inputs = {"1$ to czk","1USD CZK","1 usd kc","1USD","1 usd","1$"};
        for (final String input : usd_inputs) {
            final String result = FastMoneyConversions.process(input, false);
            System.out.println(result);
            if (firstResult == null) firstResult = result;
            Assert.assertTrue(result.startsWith("USD 1.00 = CZK "));
            Assert.assertEquals(firstResult, result);
        }
    }
}
