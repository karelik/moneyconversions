package cz.ulman.fastmoneyconversions;

import javafx.application.Application;

import java.util.List;

public interface RatesSource {

    /**
     * Loads rates from some local source
     * @param parameters app arguments
     * @return map with key - iso currency, value - rate to local currency
     */
    List<Conversion> load(Application.Parameters parameters) throws Exception;
}
