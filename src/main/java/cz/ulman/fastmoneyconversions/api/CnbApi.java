package cz.ulman.fastmoneyconversions.api;

import cz.ulman.fastmoneyconversions.*;
import javafx.application.Application;
import lombok.Data;
import lombok.val;
import org.joda.money.CurrencyUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.*;

public class CnbApi implements RatesSource {
    private static final CurrencyUnit SOURCE = CurrencyUnit.of("CZK");
    private final Kurzy kurzy;

    public CnbApi() {
        try {
            // load actual exchange rates
            val url = new URL("http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.xml");

            val jaxbContext = JAXBContext.newInstance(Kurzy.class);
            val jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            this.kurzy = (Kurzy) jaxbUnmarshaller.unmarshal(url);
            System.out.println("Hit " + url.toString());
        } catch (MalformedURLException | JAXBException e){
            throw new RuntimeException(e);
        }
    }

    public Conversion getConversion(CurrencyUnit in, CurrencyUnit out) {
        final List<Conversion> conversions = load(Stream.of(in, out).collect(Collectors.toSet()));
        return conversions.stream()
                .filter(c -> c.getSource().equals(in) && c.getTarget().equals(out))
                .findFirst()
                .orElse(null);
    }

    /**
     * @param args interested currencies
     * @return
     */
    public List<Conversion> load(Application.Parameters args) {
        return loadInternal(args.getRaw().stream().map(CurrencyUnit::of).collect(Collectors.toSet()));
    }

    public List<Conversion> load(Set<CurrencyUnit> currencies) {
        return loadInternal(currencies);
    }

    private List<Conversion> loadInternal(Set<CurrencyUnit> currencies) {
        return covert(currencies);
    }

    private List<Conversion> covert(Set<CurrencyUnit> currencies) {
        val list = new ArrayList<Conversion>(currencies.size() * 2);
        for (val r : kurzy.getTabulka().getRows()){
            val curr = CurrencyUnit.of(r.getKod());
            if (currencies.contains(curr)) {
                list.add(new Conversion(curr, SOURCE, r.getKurz()));
            }
        }
        for (val r : kurzy.getTabulka().getRows()){
            val curr = CurrencyUnit.of(r.getKod());
            if (currencies.contains(curr)) {
                list.add(new Conversion(SOURCE, curr, 1 / r.getKurz())); // this is not true of course, only for orientation
            }
        }
        return list;
    }

    public static class CzDoubleAdapter extends XmlAdapter<String, Double> {
        @Override
        public Double unmarshal(String v) {
            return Double.parseDouble(v.replace(",","."));
        }

        @Override
        public String marshal(Double v) {
            return v.toString();
        }
    }

    @Data
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement
    private static class Kurzy {
        @XmlAttribute
        private String banka;
        @XmlElement
        private Tabulka tabulka;
        @XmlAttribute
        private int poradi;
        @XmlAttribute
        @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
        private LocalDate datum;
    }

    public static class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
        private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        public LocalDate unmarshal(String v) {
            return LocalDate.parse(v, FORMATTER);
        }

        public String marshal(LocalDate v) {
            return v.format(FORMATTER);
        }
    }

    @Data
    @XmlAccessorType(XmlAccessType.FIELD)
    private static class Radek {
        @XmlAttribute
        private String kod;
        @XmlAttribute
        private String mena;
        @XmlAttribute
        private int mnozstvi;
        @XmlAttribute
        @XmlJavaTypeAdapter(value = CzDoubleAdapter.class)
        private Double kurz; // it is double
        @XmlAttribute
        private String zeme;
    }

    @Data
    @XmlAccessorType(XmlAccessType.FIELD)
    private static class Tabulka {
        @XmlAttribute
        private String typ;
        @XmlElement(name = "radek")
        private List<Radek> rows = new LinkedList<>();
    }
}
