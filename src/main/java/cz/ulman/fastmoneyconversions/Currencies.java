package cz.ulman.fastmoneyconversions;

import lombok.*;
import org.joda.money.*;

import java.util.*;

class Currencies {
    private static final Map<CurrencyUnit,Set<String>> CURRENCY_UNIT_MAP = new HashMap<>();
    static final Set<Character> CHARACTERS = new HashSet<>();

    static {
        for (val cu : CurrencyUnit.registeredCurrencies()){
            final Set<String> kws = new HashSet<>();
            kws.add(cu.getCode());
            CURRENCY_UNIT_MAP.put(cu, kws);
        }
        CURRENCY_UNIT_MAP.get(CurrencyUnit.EUR).addAll(Arrays.asList("E","€"));
        CURRENCY_UNIT_MAP.get(CurrencyUnit.USD).addAll(Arrays.asList("S","$"));
        CURRENCY_UNIT_MAP.get(CurrencyUnit.GBP).addAll(Arrays.asList("L","£"));
        CURRENCY_UNIT_MAP.get(CurrencyUnit.JPY).addAll(Arrays.asList("Y","¥"));
        CURRENCY_UNIT_MAP.get(CurrencyUnit.of("CZK")).addAll(Arrays.asList("kč", "kc"));
        CURRENCY_UNIT_MAP.get(CurrencyUnit.of("PLN")).addAll(Arrays.asList("zł","zl"));
        CHARACTERS.addAll(Arrays.asList('€','$','¥','£','ł'));
    }

    static CurrencyUnit getCurrencyUnit(String input){
        for (val e : CURRENCY_UNIT_MAP.entrySet()){
            for (val kw : e.getValue()){
                if (input.equalsIgnoreCase(kw)) return e.getKey();
            }
        }
        throw new IllegalCurrencyException("No currency for " + input);
    }

}
