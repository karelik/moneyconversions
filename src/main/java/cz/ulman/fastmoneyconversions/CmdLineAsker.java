package cz.ulman.fastmoneyconversions;

public class CmdLineAsker {
    public static void main(String[] args) {
        System.out.println(FastMoneyConversions.process(String.join(" ", args), true));
    }
}
