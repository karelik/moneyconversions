package cz.ulman.fastmoneyconversions;

import cz.ulman.fastmoneyconversions.api.*;
import lombok.*;

import java.io.*;

public class FastMoneyConversionsConsole {
    public static void main(final String[] args) throws IOException {
        val valueOnly = args.length > 0 && args[0].equals("-c");

        val core = new CnbApi();

        val console = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Query:");
        String query = console.readLine();
        while(!query.isEmpty() && !query.equalsIgnoreCase("exit")) {
            val result = FastMoneyConversions.process(core, query, valueOnly);
            System.out.println(result);
            System.out.println("Next? (Enter to exit)");
            query = console.readLine();
        }
        console.close();
    }
}
