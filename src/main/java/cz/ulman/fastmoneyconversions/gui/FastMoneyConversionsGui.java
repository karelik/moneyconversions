package cz.ulman.fastmoneyconversions.gui;

import cz.ulman.fastmoneyconversions.api.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;

public class FastMoneyConversionsGui extends Application {
    public void start(Stage stage) {
        // preparations
        val cnbApi = new CnbApi();
        val conversions = cnbApi.load(getParameters());
        if (conversions.isEmpty()) Platform.exit(); // nothing to do

        // go to launch
        val conversionList = conversions.stream()
                .map(ConversionRow::new)
                .collect(Collectors.toList());
        stage.setTitle("Currency converter");
        stage.setResizable(false);
        val gridPane = new GridPane();
        gridPane.setHgap(15d);
        gridPane.setVgap(5d);

        int row = 0;
        gridPane.add(new Label("Amount"),0, row++);
        gridPane.add(createTextField(conversionList),1,0);
        for (val conversionRow : conversionList) {
            gridPane.add(conversionRow.getLabel(), 0, row);
            gridPane.add(conversionRow.getTextField(), 1, row++);
        }
        val root = new Group();
        root.getChildren().add(gridPane);
        val scene = new Scene(root);
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();
    }

    private TextField createTextField(final List<ConversionRow> conversionList){
        val tfValue = new TextField();
        tfValue.textProperty().addListener((observable, oldValue, newValue) -> {
            double targetVal;
            try {
                targetVal = Double.parseDouble(newValue.trim().replace(",", "."));// tolerate czech comma too);
            } catch (Exception e){
                targetVal = Double.NaN;
            }
            for (val conversionRow : conversionList) {
                conversionRow.update(targetVal);
            }
        });
        tfValue.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                Platform.exit();
            }
        });
        return tfValue;
    }

    public static void main(final String[] args) {
        launch(args);
    }
}
