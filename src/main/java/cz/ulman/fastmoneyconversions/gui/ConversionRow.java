package cz.ulman.fastmoneyconversions.gui;

import cz.ulman.fastmoneyconversions.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.Getter;
import org.joda.money.Money;

import java.text.NumberFormat;

class ConversionRow {
    private static final int MAX_FRACTION_DIGITS = 5;

    private final NumberFormat nf = NumberFormat.getInstance();

    private final Conversion conversion;
    @Getter
    private final TextField textField;
    @Getter
    private final Label label;

    ConversionRow(Conversion conversion) {
        // creates gui objects connected with formula
        label = new Label(conversion.getLabel());
        this.conversion = conversion;

        // result text field
        textField = new TextField();
        update(Double.NaN);
        textField.setEditable(false);

        // initial actions
        nf.setMaximumFractionDigits(MAX_FRACTION_DIGITS);
    }

    /**
     * Calculates result of formula with changed variable value.
     *
     * @param number
     * @return
     */
    private String getResult(double number) {
        return nf.format(conversion.getResult(Money.of(conversion.getSource(), number)).getAmount());
    }

    void update(double number) {
        if (Double.isNaN(number)) textField.setText("N/A");
        else textField.setText(getResult(number));
    }
}
