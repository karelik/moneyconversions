package cz.ulman.fastmoneyconversions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.math.BigDecimal;
import java.math.RoundingMode;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Conversion {
    private final CurrencyUnit source;
    private final CurrencyUnit target;
    private final double rate;

    public String getLabel(){
        return source + " to " + target;
    }

    public Money getResult(Money in) {
        return in.convertedTo(target, BigDecimal.valueOf(rate), RoundingMode.HALF_UP);
    }
}
