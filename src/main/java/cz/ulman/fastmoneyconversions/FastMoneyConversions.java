package cz.ulman.fastmoneyconversions;

import cz.ulman.fastmoneyconversions.api.*;
import lombok.*;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.util.*;

public class FastMoneyConversions {

    public static String process(final String query, final boolean valueOnly) {
        return process(null, query, valueOnly);
    }

    public static String process(final CnbApi core, final String query, final boolean valueOnly) {
        val stringScanner = new StringScanner(query);
        val amount = Double.parseDouble(findNumber(stringScanner));
        var in = Currencies.getCurrencyUnit(findCurrencyUnit(stringScanner));
        removeTo(stringScanner);
        val out = stringScanner.hasNext() ? Currencies.getCurrencyUnit(findCurrencyUnit(stringScanner)) : CurrencyUnit.of(Locale.getDefault());
        return getResult(core, Money.of(in, amount), out, valueOnly);
    }

    private static String getResult(final CnbApi core, final Money in, final CurrencyUnit out, final boolean valueOnly) {
        final CnbApi coreInternal = core == null ? new CnbApi() : core;
        val conversion = coreInternal.getConversion(in.getCurrencyUnit(), out);
        if (conversion == null) return null;

        val result = String.format("%s = %s", in, conversion.getResult(in));
        if (valueOnly) {
            val pieces = result.split(" = ");
            val parts = pieces[1].split(" ");
            return parts[1];
        }
        return result;
    }

    private static String findNumber(final StringScanner characters){
        val temp = new StringBuilder();
        while (characters.hasNext()) {
            val character = characters.next();
            if (Character.isDigit(character) || character == '.' || character == ','){
                if (character == ','){
                    temp.append('.'); // swap comma to dot
                } else {
                    temp.append(character);
                }
            } else if (Character.isSpaceChar(character)){
                break; // remove space
            } else {
                characters.back(); // put back
                break;
            }
        }
        return temp.toString();
    }

    private static String findCurrencyUnit(final StringScanner characters){
        val temp = new StringBuilder();
        while (characters.hasNext()) {
            val character = characters.next();
            if (Character.isLetter(character) || Currencies.CHARACTERS.contains(character)){
                temp.append(character);
            } else if (Character.isSpaceChar(character)){
                break;
            } else {
                characters.back(); // put back
                break;
            }
        }
        return temp.toString();
    }

    private static void removeTo(final StringScanner characters){
        while (characters.hasNext()) {
            val character = characters.next();
            if (character == 't' || character == 'o' || Character.isSpaceChar(character)){
                // ignore it and remove
            } else {
                characters.back(); // put back
                break;
            }
        }
    }
}
