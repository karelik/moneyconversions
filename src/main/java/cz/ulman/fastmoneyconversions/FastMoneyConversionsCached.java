package cz.ulman.fastmoneyconversions;

import cz.ulman.fastmoneyconversions.api.*;
import lombok.*;

import java.util.*;

public class FastMoneyConversionsCached {
    private final Map<String,String> valueOnlyCache = new HashMap<>();
    private final Map<String,String> normalCache = new HashMap<>();
    private final CnbApi api = new CnbApi();

    public String process(final String query, final boolean valueOnly) {
        val cache = valueOnly ? valueOnlyCache : normalCache;
        return cache.computeIfAbsent(query, v -> FastMoneyConversions.process(api, query, valueOnly));
    }
}
