package cz.ulman.fastmoneyconversions;

import java.util.*;

public class StringScanner implements Iterator<Character> {
    private final String string;
    private int index;

    public StringScanner(final String string) {
        this.string = string;
        this.index = -1;
    }

    @Override
    public boolean hasNext() {
        return (index + 1) < string.length();
    }

    @Override
    public Character next(){
        try {
            forward();
            return string.charAt(index);
        } catch (IndexOutOfBoundsException e){
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public void forward(){
        index++;
    }

    public void back(){
        index--;
    }
}
